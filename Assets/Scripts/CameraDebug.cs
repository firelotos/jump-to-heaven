﻿using UnityEngine;
using System.Collections;

//This class ned for debug camera
public class CameraDebug : MonoBehaviour {
	GameObject crusader;
	Camera mainCamera;
	float angle;

	void Start () {
		crusader = GameObject.Find ("Crusader");
		mainCamera = Camera.main;
		angle = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (UnityEngine.Debug.isDebugBuild) {
			if (Input.GetAxis ("Mouse ScrollWheel") < 0) { // back
				mainCamera.orthographicSize = mainCamera.orthographicSize + 1;
			} else if (Input.GetAxis ("Mouse ScrollWheel") != 0) { // forward
				mainCamera.orthographicSize = mainCamera.orthographicSize - 1;
			}

			if (Input.GetKey(KeyCode.Q)) {
				angle += 45 * Time.deltaTime;
			}
			if (Input.GetKey(KeyCode.E)) {
				angle -= 45 * Time.deltaTime;
			}
			transform.eulerAngles = new Vector3(0, 0, angle);
		}

		if (!Level.IsPlayerDied) {
			float y = gameObject.transform.position.y;
			if (crusader.transform.position.y > gameObject.transform.position.y - crusader.transform.localScale.y) {
				y = crusader.transform.position.y + crusader.transform.localScale.y;
			}

			gameObject.transform.position = new Vector3 (crusader.transform.position.x, y, gameObject.transform.position.z);
		}
	}
}