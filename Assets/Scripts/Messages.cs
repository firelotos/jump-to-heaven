using System;
using System.IO;
using System.Collections.Generic;

public static class Messages
{
	public static List<string> MessagesList = Init();
	public static List<bool> Was;
	
	public const int Rock = 0,
					 Meteor = 1,
					 Ice = 2,
					 Flash = 3;

	private static List<string> Init() {
		List<string> res = new List<string> ();
		Was = new List<bool> ();
		StreamReader sr = new StreamReader ("Phrases.txt");
		string Message = sr.ReadLine ();
		while (Message != null) {
			res.Add(Message);
			Message = sr.ReadLine();
		}
		for (int i = 0; i < res.Count; i++) {
			Was.Add(i < 3);
		}
		return res;
	}

	public static int GetNextMessage() {
		for (int i = 0; i < MessagesList.Count; i++) {
			if (!Was[i]) {
				Was[i] = true;
				return i;
			}
		}
		for (int i = 0; i < MessagesList.Count; i++) {
			Was[i] = i < 3;
		}
		return 0;
	}

	public static string GetMessage() {
		int n = Level.BotMessageNumber;
		if (MessagesList [n].StartsWith ("-")) {
			return MessagesList [n].Split('|')[0].Remove(0, 1).Replace('n', '\n');
		} else
			return MessagesList [n].Remove(0, 1).Replace('n', '\n');
	}

	public static string GetAddon() {
		int n = Level.BotMessageNumber;
		if (MessagesList[n].StartsWith("-")) {
			return MessagesList [n].Split ('|') [1].Replace('n', '\n');
		} else
			return null;
	}
}