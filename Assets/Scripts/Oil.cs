﻿using UnityEngine;
using System.Collections;

public class Oil : MonoBehaviour {
	public GameObject OilDrop;
	public GameObject Oilled;
	
	public void Start () {
		OilDrop = GameObject.Find ("OilDrop");
		Oilled = GameObject.Find ("Oilled");
		if (gameObject.name.Contains("lone"))
			InvokeRepeating("Decor", 0, 0.3f);
	}
	
	public void OnTriggerEnter2D(Collider2D coll) {
		if (coll.name.StartsWith ("Gra")) {
			Vector3 pos = coll.gameObject.transform.position;
			Destroy (coll.gameObject);
			Instantiate (Oilled, pos, new Quaternion (0, 0, 0, 0));
		}
	}
	
	public void Decor() {
		Vector3 pos = Random.insideUnitSphere * 4 + gameObject.transform.position;
		Instantiate (OilDrop, pos, new Quaternion(90, 0, 0, 0));
	}
}
