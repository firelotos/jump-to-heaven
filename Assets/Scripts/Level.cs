using System;
using UnityEngine;

public static class Level
{
	public static ArtificialIntelligence AI;
	public static Weapons ChoosedWeapon = Weapons.Stone;
	//												   S  M  O  I  M
	//												   T  E  I  C  I
	//												   O  T  L  E  N
	//												   N  E        E
	//												   E  O         
	//												      R
	//
	public static int[] WeaponsLevel = new int[5]    { 1, 0, 0, 0, 0 };
	public static int[] WeaponsMaxLevel = new int[5] { 3, 3, 1, 1, 3 };
	public static float[] ColdDown = new float[5]    { 0, 0, 0, 0, 0 };
	public const  string PlatformTag = "Respawn";
	public const  int maxPath = 25,
					  minPath = 20;
	public static GameObject lastPlatform = GameObject.Find("Platform"),
							 chest = GameObject.Find("Chest"),
							 grass = GameObject.Find ("Grass");
	public static System.Random rand = new System.Random();
	public static bool IsPlayerDied = false, menu = false, showMessage = false;
	public static int PlatformsCount = 1, BotMessageNumber = 0, expPoints = 0, mana = 200;
	public static int Jumped = 0, LoadedLevel;

	public static void SpawnPlatform(GameObject platform, bool spawnChest)
	{
		if (PlatformsCount < 10 && platform) {
			PlatformsCount++;
			lastPlatform = platform;
			Vector3 pos = RandomNear (lastPlatform);
			lastPlatform = (GameObject)GameObject.Instantiate (lastPlatform, pos, new Quaternion (0, 0, 0, 0));
			lastPlatform.tag = PlatformTag;
			((GameObject)GameObject.Instantiate (grass, pos, new Quaternion(0, 0, 0, 0))).tag = "Respawn";
			if (spawnChest && chest)
				((GameObject)GameObject.Instantiate (chest, pos, new Quaternion(0, 0, 0, 0))).tag = "Respawn";
		}
	}
	
	public static void SpawnPlatform(bool spawnChest)
	{
		SpawnPlatform (lastPlatform, spawnChest);
	}

	public static string WeaponSizeToString() {
		switch (WeaponsLevel[(int)ChoosedWeapon]) {
		case 1:
			return "Small";
		case 2:
			return "Middle";
		case 3:
			return "Big";
		}
		return "";
	}

	public static GameObject[] GetPlatforms() {
		return GameObject.FindGameObjectsWithTag (PlatformTag);
	}
	
	public static void RemoveAllGenerated() {
		Level.PlatformsCount = 1;
		GameObject[] ps = GetPlatforms();
		foreach (GameObject p in ps) {
			GameObject.Destroy(p);
		}
	}

	public static string GetWeaponName() {
		switch (ChoosedWeapon) {
		case Weapons.Mine:
			ColdDown[(int)ChoosedWeapon] = 5f;
			return "Mine" + WeaponSizeToString ();
		case Weapons.Ice:
			ColdDown[(int)ChoosedWeapon] = 4f;
			return "Ice";
		case Weapons.Meteor:
			ColdDown[(int)ChoosedWeapon] = 2f;
			return "Meteor" + WeaponSizeToString ();
		case Weapons.Oil:
			ColdDown[(int)ChoosedWeapon] = 3f;
			return "Oil";
		case Weapons.Stone:
			ColdDown[(int)ChoosedWeapon] = 1f;
			return "Stone" + WeaponSizeToString ();
		}
		return ChoosedWeapon.ToString () + WeaponSizeToString ();
	}

	public static float RandomFloat(float min, float max)
	{
		float res = (float)min + Level.rand.Next ((int)(100 * (max - min))) / 100;
		return res;
	}
	
	public static Vector3 RandomNear(GameObject gameObject) {
		try {
			Vector3 res = gameObject.transform.position;
			/*				SIRCLE (was in alpha 0.1)
			float Angle = RandomFloat (-Mathf.PI / 2, Mathf.PI / 2);
			res.x += (Level.rand.Next(100) > 49 ? -1 : 1) * Mathf.Sin(Angle) * Radius;
			res.y += Mathf.Cos(Angle) * Radius;     */
			res.x += RandomFloat (minPath, maxPath) * (rand.Next(100) > 50 ? 1 : -1);
			/*			  RANDOM H (was in alpha 0.2)
			res.y += RandomFloat (minPath, maxPath);*/
			res.y += maxPath;
			return res;
		} catch {
			return Vector3.zero;
		}
	}

	public static void print(object o){
		MonoBehaviour.print (o);
	}
}