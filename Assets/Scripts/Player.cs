using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	public Texture2D ButtonPressedIcon, ButtonUnpressedIcon, stone, meteor, oil, ice, mine;
	public GUIStyle labelStyle, coldDownStyle;
	public bool wasMousePressed;
	public Vector3 mousePressPosition;
	public GameObject HealtBar, OpacityBar, ManaBar;

	public void Start () {
		wasMousePressed = false;
		mousePressPosition = Vector3.zero;
		HealtBar = GameObject.Find ("HealthValue");
		OpacityBar = GameObject.Find ("OpacityValue");
		ManaBar = GameObject.Find ("ManaValue");
	}

	public void Update () {
		if (Level.IsPlayerDied)
				return;
		for (int i = 0; i < Level.ColdDown.Length; i++) {
			if (Level.ColdDown[i] > 0)
				Level.ColdDown[i] -= Time.deltaTime;
		}
		if (Level.mana < 200) {
			Level.mana += (int)(Time.deltaTime * 100);
		} else {
			Level.mana = 200;
		}	
		HealtBar.transform.localScale = new Vector3 (HealtBar.transform.localScale.x,
		                                             7.12f / Level.AI.MaxHealth * Level.AI.Health);
		OpacityBar.transform.localScale = new Vector3 (OpacityBar.transform.localScale.x,
		                                               4.13f / Level.AI.MaxOpacity * Level.AI.Opacity);
		ManaBar.transform.localScale = new Vector3 (ManaBar.transform.localScale.x,
		                                               4.13f / 200 * Level.mana);
		bool isMousePressed = Input.GetMouseButton (0);
		if (wasMousePressed && !isMousePressed) {
			OnMouseUp();
		} else if (!wasMousePressed && isMousePressed) {
			OnMouseDown();
		}
		wasMousePressed = isMousePressed;

		if (Input.GetKeyDown (KeyCode.Escape)) {
			Level.menu = !Level.menu;
		}
		if (Level.menu && Time.timeScale > 0) {
			Time.timeScale = 0;
		} else if (!Level.menu && Time.timeScale < 1) {
			Time.timeScale = 1;
			Level.showMessage = false;
		}

		#region Hotkeys
		if (Input.GetKey(KeyCode.Alpha1)) {
			TrySetWeapon(Weapons.Stone);
		}
		if (Input.GetKey(KeyCode.Alpha2)) {
			TrySetWeapon(Weapons.Meteor);
		}
		if (Input.GetKey(KeyCode.Alpha3)) {
			TrySetWeapon(Weapons.Oil);
		}
		if (Input.GetKey(KeyCode.Alpha4)) {
			TrySetWeapon(Weapons.Ice);
		}
		if (Input.GetKey(KeyCode.Alpha5)) {
			TrySetWeapon(Weapons.Mine);
		}
		#endregion
	}
	
	public void OnMouseDown() {
		mousePressPosition = Input.mousePosition;
		mousePressPosition = new Vector3 (mousePressPosition.x, mousePressPosition.y, -5);
	}
	
	public void OnMouseUp() {
		if (Level.menu || mousePressPosition.y <= 167 || Level.ColdDown[(int)Level.ChoosedWeapon] > 0 ||
		    			  Level.mana < 10) {
			return;
		}
		Level.mana -= 10;
		GameObject original = GameObject.Find(Level.GetWeaponName());
		Vector3 pos = Camera.main.ScreenToWorldPoint (mousePressPosition);
		pos = new Vector3 (pos.x, pos.y, 0);
		GameObject stone = Instantiate (original, pos, new Quaternion (0, 0, 0, 0)) as GameObject;
		stone.rigidbody2D.WakeUp ();
		Vector3 force = (mousePressPosition - Input.mousePosition) * 2;
		stone.rigidbody2D.AddForce (new Vector2 (force.x, force.y));
	}

	public void OnGUI() {
		GUI.skin.button.normal.background = ButtonUnpressedIcon;
		GUI.skin.button.hover.background = ButtonUnpressedIcon;
		GUI.skin.button.active.background = ButtonPressedIcon;
		#region GUI elements
		GUI.Label(new Rect (0, Screen.height - 75, 100, 50), Level.Jumped.ToString(), coldDownStyle);
		float btnSize = 61, bthY = 688, p = 5;
		if (GUI.Button (new Rect (207, bthY, btnSize, btnSize), stone)) {
			TrySetWeapon(Weapons.Stone);
		}
		if (Level.ColdDown [(int)Weapons.Stone] > 0) {
			GUI.Label (new Rect (207, bthY, btnSize, btnSize), 
			           new GUIContent ((1 + (int)Level.ColdDown [(int)Weapons.Meteor]).ToString ()), coldDownStyle);
		}
		if (GUI.Button (new Rect (207 + btnSize + 5, bthY, btnSize, btnSize), meteor)) {
			TrySetWeapon (Weapons.Meteor);
		}
		if (Level.ColdDown [(int)Weapons.Meteor] > 0) {
			GUI.Label (new Rect (207 + btnSize + 5, bthY, btnSize, btnSize), 
		          new GUIContent ((1 + (int)Level.ColdDown [(int)Weapons.Meteor]).ToString ()), coldDownStyle);
		}
		if (GUI.Button (new Rect (207 + (p + btnSize) * 2, bthY, btnSize, btnSize), oil)) {
			TrySetWeapon (Weapons.Oil);
		}
		if (Level.ColdDown [(int)Weapons.Oil] > 0) {
			GUI.Label (new Rect (207 + (p + btnSize) * 2, bthY, btnSize, btnSize), 
		          new GUIContent ((1 + (int)Level.ColdDown [(int)Weapons.Oil]).ToString ()), coldDownStyle);
		}
		if (GUI.Button (new Rect (207 + (p + btnSize) * 3, bthY, btnSize, btnSize), ice)) {
			TrySetWeapon (Weapons.Ice);
		}
		if (Level.ColdDown [(int)Weapons.Ice] > 0) {
			GUI.Label (new Rect (207 + (p + btnSize) * 3, bthY, btnSize, btnSize), 
		          new GUIContent ((1 + (int)Level.ColdDown [(int)Weapons.Ice]).ToString ()), coldDownStyle);
		}
		if (GUI.Button (new Rect (207 + (p + btnSize) * 4, bthY, btnSize, btnSize), mine)) {
			TrySetWeapon (Weapons.Mine);
		}
		if (Level.ColdDown [(int)Weapons.Mine] > 0) {
			GUI.Label (new Rect (207 + (p + btnSize) * 4, bthY, btnSize, btnSize), 
		          new GUIContent ((1 + (int)Level.ColdDown [(int)Weapons.Mine]).ToString ()), coldDownStyle);
		}
		if (GUI.Button (new Rect (207 + (p + btnSize) * 5, bthY, btnSize, btnSize), Level.expPoints.ToString())) {

		}
		#endregion
		GUI.Label (new Rect (895, 680, 100, 40), "X" + Level.AI.HealthPotions.ToString (), labelStyle);
		GUI.Label (new Rect (895, 725, 100, 40), "X" + Level.AI.OpacityPotions.ToString (), labelStyle);
	}

	public void TrySetWeapon(Weapons w) {
		if (!Level.menu && Level.WeaponsLevel[(int)w] > 0) {
			Level.ChoosedWeapon = w;
		}
	}
}
