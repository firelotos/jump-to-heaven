﻿using UnityEngine;
using System.Collections;

public class PlatformSpawner : MonoBehaviour {
	public const int ChestChance = 40;

	public void Start () {
		Level.SpawnPlatform (gameObject, Level.rand.Next(100) < ChestChance);
	}
	
	public void OnBecameInvisible()
	{
		if (gameObject.name != "Platform") {
			Level.SpawnPlatform (Level.rand.Next(100) < ChestChance);
			Level.PlatformsCount--;
		}
	}

	public void Update () {
	}
}
