﻿using UnityEngine;
using System;

public class BotMind : MonoBehaviour {
	public string StoneMessage, MeteorMessage;
	public SpriteRenderer sr;
	public GUIStyle MessageStyle;

	public void Start () {
		sr = GetComponent<SpriteRenderer> ();
	}

	public void Update () {
		if (Level.BotMessageNumber > -1) {
			if (Input.GetMouseButtonDown(0) && Messages.GetAddon() != null &&	
			    Input.mousePosition.x >= 736 && Input.mousePosition.x <= 916 &&
			    Input.mousePosition.x >= 127 && Input.mousePosition.y <= 231) {
				Level.menu = true;
				Level.showMessage = true;
			}
			if (sr.color.a < 1) {
				sr.color = new Color(255, 255, 255, sr.color.a + Time.deltaTime * 5);
			}
		} else {
			if (sr.color.a > 0) {
				sr.color = new Color(255, 255, 255, sr.color.a - Time.deltaTime * 5);
			}
		}
	}

	public void OnGUI() {
		if (Level.BotMessageNumber > -1) {
			GUI.Label(new Rect(765, 540, 130, 100), Messages.GetMessage(), MessageStyle);
		}
	}
}
