﻿using UnityEngine;
using System.Collections;

public class OnDeath : MonoBehaviour {
	public string OldRecord, NewRecord, Win, Again;
	public GUIStyle st;
	public SpriteRenderer sp;

	public void Start () {
		sp = gameObject.GetComponent<SpriteRenderer> ();
	}
	
	public void Update () {
		Level.AI.Health = Mathf.Max (Level.AI.Health, 0);
		if (Level.IsPlayerDied) {
			Records.AddRecord();
			if (sp.color.a < 255) {
				sp.color = new Color(255, 0 , 0, sp.color.a + Time.deltaTime);
				Level.AI.Health = 0;
			}
			if (Input.anyKeyDown) {
				GameObject[] respawn = GameObject.FindGameObjectsWithTag("Respawn");
				for (int i = 0; i < respawn.Length; i++) {
					Destroy(respawn[i]);
				}
				Application.Quit();
			}
		} else {
			if (sp.color.a > 0)
				sp.color = new Color(255, 0 , 0, sp.color.a - Time.deltaTime * 3);
		}
	}

	public void OnGUI() {
		if (Level.IsPlayerDied) {
			string s = "";
			if (Records.RecordsList.Count > 0) {
				s = OldRecord + Records.RecordsList[0].ToString() + '\n';
				s += NewRecord + Level.Jumped;
			} else {
				s = NewRecord + Level.Jumped;
			}
			GUI.Label(new Rect(0, 0, Screen.width, Screen.height), s, st);
		}
	}
}
