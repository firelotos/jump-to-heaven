﻿using UnityEngine;
using System.Collections;

public class Autoremover : MonoBehaviour {
	public bool isClone;

	public void Start () {
		isClone = gameObject.name.Contains ("lone");
	}

	public void Update () {
		if (isClone && Camera.main.WorldToScreenPoint (gameObject.transform.position).y < 0)
			Destroy (gameObject);
	}

	public void OnApplicationQuit() {
		if (isClone)
		Destroy(gameObject);
	}
}
