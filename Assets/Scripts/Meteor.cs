﻿using UnityEngine;
using System.Collections;

public class Meteor : MonoBehaviour {
	public GameObject Fire;
	public GameObject Fired;

	public void Start () {
		Fire = GameObject.Find ("FireParticle");
		Fired = GameObject.Find ("Fired");
		if (gameObject.name.Contains("lone"))
			InvokeRepeating("Decor", 0, 0.1f);	
	}
	
	public void OnTriggerEnter2D(Collider2D coll) {
		if (coll.name.StartsWith ("Gra")) {
			Vector3 pos = coll.gameObject.transform.position;
			Destroy (coll.gameObject);
			Instantiate (Fired, pos, new Quaternion (0, 0, 0, 0));
		}
	}
	
	public void Decor() {
		Instantiate (Fire, Random.insideUnitSphere * 4 + gameObject.transform.position, new Quaternion (0, 0, 0, 0));
	}
}
