﻿using UnityEngine;
using System.Collections;

public class SkillMenu : MonoBehaviour {
	public Texture2D ButtonUnpressedIcon, ButtonPressedIcon, Arrow;
	public Texture2D stone, meteor, mine, oil, ice, corruption;
	public GUIStyle TitleStyle, MessageStyle;
	public string MenuName;
	public SpriteRenderer sr;

	public void Start () {
		sr = gameObject.GetComponent<SpriteRenderer> ();
	}

	public void Update () {
	
	}

	public void OnGUI() {
		if (Level.menu) {
			sr.color = new Color (255, 255, 255, 255);
			GUI.skin.button.normal.background = ButtonUnpressedIcon;
			GUI.skin.button.hover.background = ButtonUnpressedIcon;
			GUI.skin.button.active.background = ButtonPressedIcon;
			if (Level.showMessage) {
				GUI.Label(new Rect(390, 150, 250, 400), Messages.GetAddon());
				GUI.Label(new Rect(390, 110, 250, 20), Messages.GetMessage(), TitleStyle);
			} else {
				GUI.Label(new Rect(390, 110, 250, 20), MenuName, TitleStyle);
				#region Buttons and arrows
				float btnSize = 70, p = 34;
				#region First
				if (GUI.Button (new Rect (390, 160, btnSize, btnSize), new GUIContent(GetCost(Weapons.Stone).ToString(), stone, "asdfhasjfhjsag"))) {
					Upgrate(Weapons.Stone);
				}
				GUI.DrawTexture (new Rect(390 + btnSize / 2 - Arrow.width / 2, 161 + btnSize, Arrow.width, Arrow.height), Arrow);
				if (GUI.Button (new Rect(390, 162 + btnSize + Arrow.height, btnSize, btnSize), meteor)) {
					Upgrate(Weapons.Meteor, Weapons.Stone);
				}
				GUI.DrawTexture (new Rect(390 + btnSize / 2 - Arrow.width / 2, 163 + btnSize * 2 + Arrow.height, Arrow.width, Arrow.height), Arrow);
				if (GUI.Button (new Rect(390, 164 + btnSize * 2 + Arrow.height * 2, btnSize, btnSize), mine)) {
					Upgrate(Weapons.Mine, Weapons.Meteor);
				}
				#endregion
				#region Third
				if (GUI.Button (new Rect (500 + p * 2, 160, btnSize, btnSize), ice)) {
					Upgrate(Weapons.Ice);
				}
				GUI.DrawTexture (new Rect(500 + p * 2 + btnSize / 2 - Arrow.width / 2, 161 + btnSize, Arrow.width, Arrow.height), Arrow);
				if (GUI.Button (new Rect(500 + p * 2, 162 + btnSize + Arrow.height, btnSize, btnSize), oil)) {
					Upgrate(Weapons.Oil, Weapons.Ice);
				}
				#endregion
				#endregion
			}
		} else {
			sr.color = new Color (255, 255, 255, 0);
		}
	}

	public void Upgrate(Weapons w) {
		if (Level.WeaponsMaxLevel[(int)w] == Level.WeaponsLevel[(int)w])
			return;
		if (Level.expPoints >= GetCost(w)) {
			Level.WeaponsLevel[(int)w]++;
			Level.expPoints -= GetCost(w);
		}
	}

	public int GetCost(Weapons w) {
		return ((int)w + 1) * (Level.WeaponsLevel [(int)w] + 1);
	}
	
	public void Upgrate(Weapons w, Weapons prevW) {
		if (Level.WeaponsLevel [(int)prevW] > 0)
			Upgrate (w);
	}
}
