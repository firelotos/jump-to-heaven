﻿using UnityEngine;
using System.Collections;

public class ArtificialIntelligence : MonoBehaviour {
	public AnimationClip WalkClip, JumpClip, DeathClip;
	public const float MessageTimerConst = 10f;
	public GameObject Fire;
	public float MaxHealth, MaxOpacity;
	public float Health, Opacity;
	public float JumpForce, MoveForce, FireTimer, deltaX, messageTimer, forRun;
	public int HealthPotions, OpacityPotions;
	public GameObject previousPlatform, lastPlatform;
	public Transform oldTransform;
	public Rigidbody2D rigibody;
	public bool useAI, jumped, doubleJumped;

	void Start () {
		rigibody = gameObject.GetComponent<Rigidbody2D> ();
		FireTimer = 0;
		JumpForce = 100000;
		MoveForce = 150;
		useAI = true;
		jumped = true;
		doubleJumped = true;
		deltaX = 0;
		forRun = 0;
		oldTransform = transform;
		Health = MaxHealth = 100;
		Opacity = MaxOpacity = 100;
		messageTimer = MessageTimerConst;
		HealthPotions = OpacityPotions = 0;
		lastPlatform = Level.GetPlatforms () [0];
		previousPlatform = Level.GetPlatforms () [0];
		Fire = GameObject.Find ("FireParticle");
		Level.AI = this;
	}

	public void InFire() {
		Instantiate (Fire, Random.insideUnitSphere * 5 + gameObject.transform.position, new Quaternion (0, 0, 0, 0));
		Health -= Time.deltaTime * 30;
	}

	public void OnApplicationQuit() {
		Level.RemoveAllGenerated();
	}

	public void Update () {
		if (Level.menu)
			return;
		if (forRun > 0)
			forRun -= Time.deltaTime;
		messageTimer -= Time.deltaTime;
		if (messageTimer <= 0) {
			Level.BotMessageNumber = Messages.GetNextMessage();
			messageTimer = MessageTimerConst;
		} else if (messageTimer <= (MessageTimerConst / 10 * 6)) {
			if (Level.BotMessageNumber < 2 && Level.BotMessageNumber > -1) {
				Level.BotMessageNumber++;
				messageTimer = MessageTimerConst;
			} else {
				Level.BotMessageNumber = -1;
			}
		}
		if (FireTimer - Time.deltaTime > 0)
			FireTimer -= Time.deltaTime;
		else {
			if (FireTimer > 0)
				CancelInvoke("InFire");
			else
				FireTimer = 0;
		}


		deltaX = gameObject.transform.position.x - oldTransform.position.x;
		if (Opacity < 50f && OpacityPotions > 0) {
			OpacityPotions--;
			Opacity += 50f;
		}
		if (Health < 50f && HealthPotions > 0) {
			HealthPotions--;
			Health += 50f;
		}
		if (Health < 1) {
			return;
		}
		AI ();
		Level.IsPlayerDied = Level.IsPlayerDied || Health <= 0 || Opacity <= 0;
		oldTransform = gameObject.transform;
	}

	public void AI() {	
		GameObject platform;
		if (jumped)
			platform = lastPlatform;
		else
			platform = GetNextPlatform ();
		if (Opacity >= 5) {
			Jump ();

			if (jumped && deltaX > 0) {
				DoubleJump();
			}	
			if (platform)
				if (platform.transform.position.x > gameObject.transform.position.x) {
					GoRight();
				} else {
					GoLeft();
				}
		}
	}
	
	public void OnCollisionEnter2D(Collision2D coll)
	{
		foreach (ContactPoint2D contact in coll.contacts) {
			Debug.DrawLine(contact.point, contact.point + contact.normal, Color.green, 2, false);
		}
		if (coll.gameObject.name.StartsWith("Plat")) {
			jumped = false;
			doubleJumped = false;
			forRun = 0.05f;
			animation.Play("Walk");
		} else {
			float pow = GetPower(coll.gameObject);
			Health -= pow;
			Level.expPoints += ((int)pow + 1) / 2;
			if (Opacity >= 5)
				DoubleJump();
		}
	}

	public void OnTriggerEnter2D(Collider2D coll) {
		if (coll.gameObject.name.StartsWith("Chest")) {
			if (HealthPotions > OpacityPotions) {
				OpacityPotions++;
			} else {
				HealthPotions++;
			}
		} else if (coll.gameObject.name.StartsWith("Fired")) {
			Health -= 5f;	
		} else if (coll.gameObject.name.StartsWith("Oilled")) {
			//TODO: onOilTime = 1f;
		}
	}

	public float GetPower(GameObject go) {
		switch (go.name[0]) {
		case 'S':
			switch (go.name[5]) {
			case 'B':
				return 10;
			case 'M':
				return 7;
			}
			return 3;

		case 'M':
			switch (go.name[6]) {
			case 'B':
				InvokeRepeating("InFire", 0, 0.1F);
				FireTimer = 6;
				break;
			case 'M':
				InvokeRepeating("InFire", 0, 0.1F);
				FireTimer = 4;
				break;
			case 'S':
				InvokeRepeating("InFire", 0, 0.1F);
				FireTimer = 2;
				break;
			}
			return 5;
		case 'I':
			Opacity -= 20;
			return 20;
		case 'O':
			return 3;
		}
		return 10;
	}

	public GameObject GetNextPlatform() {
		foreach (GameObject pl in Level.GetPlatforms()) {
			if (pl.transform.position.y > gameObject.transform.position.y) {
				if (!jumped) {
					previousPlatform = lastPlatform;
					lastPlatform = pl;
				}
				return pl;
			}
		}
		return Level.GetPlatforms()[0];
	}

	public void PlayerInput() {
		if (Input.GetKey (KeyCode.A)) {
			GoLeft();
		}
		if (Input.GetKey (KeyCode.D)) {
			GoRight();
		}
		
		if (Input.GetKeyDown (KeyCode.Space)) {
			jumped = false;
			Jump();
		}
	}

	public bool Jump() {
		if (!jumped && Opacity >= 5 && forRun <= 0) {
			Level.Jumped++;
			animation.Play("Jump");
			rigibody.AddForce(new Vector2(0, JumpForce));
			Opacity -= 5;
			jumped = true;
			doubleJumped = true;
			return true;
		}
		return false;
	}

	public void DoubleJump() {
		if (!doubleJumped) {
			Opacity -= 5;
			doubleJumped = true;
			Jump ();
		}
	}

	public void GoRight() {
		rigibody.AddForce(new Vector2(Mathf.Abs(transform.position.x - lastPlatform.transform.position.x) * 10, 0));
		gameObject.transform.rotation = new Quaternion (0, 0, 0, 0);
	}

	public void GoLeft() {
		rigibody.AddForce(new Vector2(-Mathf.Abs(transform.position.x - lastPlatform.transform.position.x) * 10, 0));
		gameObject.transform.rotation = new Quaternion (0, -180, 0, 0);
	}

	public void OnBecameInvisible()
	{
		Level.IsPlayerDied = true;
	}
}