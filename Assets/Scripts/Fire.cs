﻿using UnityEngine;
using System.Collections;

public class Fire : MonoBehaviour {
	public float Alive;
	public GameObject Fired;

	public void Start () {
		Alive = 2f;
		Fired = GameObject.Find ("Fired");
	}
	
	public void OnTriggerEnter2D(Collider2D coll) {
		if (coll.name.StartsWith ("Gra")) {
			Vector3 pos = coll.gameObject.transform.position;
			Destroy (coll.gameObject);
			Instantiate (Fired, pos, new Quaternion (0, 0, 0, 0));
		}
	}

	public void Update () {
		if (!gameObject.name.Contains("lone"))
			return;
		Alive -= Time.deltaTime;
		if (Alive < 0)
			Destroy (gameObject);
	}
}
