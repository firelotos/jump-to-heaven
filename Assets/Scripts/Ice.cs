﻿using UnityEngine;
using System.Collections;

public class Ice : MonoBehaviour {
	public GameObject Iced;
	
	public void Start () {
		Iced = GameObject.Find ("Iced");
	}
	
	public void OnTriggerEnter2D(Collider2D coll) {
		if (coll.name.StartsWith ("Gra")) {
			Vector3 pos = coll.gameObject.transform.position;
			Destroy (coll.gameObject);
			Instantiate (Iced, pos, new Quaternion (0, 0, 0, 0));
		}
	}
}
