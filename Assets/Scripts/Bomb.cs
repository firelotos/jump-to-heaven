﻿using UnityEngine;
using System.Collections;

public class Bomb : MonoBehaviour {
	public float aliveTime;
	public bool Boomed;

	public void Start () {
		aliveTime = 3f;
		Boomed = false;
	}

	public void Update () {
		if (name.Contains ("lone")) {
			aliveTime -= Time.deltaTime;
			int k = 100;
			switch (name[4]) {
			case 'B':
				k = (int)(k * 2.5f);
				break;
			case 'M':
				k = (int)(k * 1.5f);
				break;
			}
			if (aliveTime <= 0f && !Boomed) {
				Destroy(gameObject);
				Boomed = true;
				Level.AI.rigibody.AddForce (k * (Level.AI.transform.position - transform.position));
			}

		}
	}
}
