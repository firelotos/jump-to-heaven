using System;

public enum Weapons
{
	Stone = 0,
	Meteor = 1,
	Oil = 2,
	Ice = 3,
	Mine = 4
}